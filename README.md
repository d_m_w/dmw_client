# DMW SDK para PHP

Esta biblioteca fornece aos desenvolvedores um conjunto simples de ligações para ajudá-lo a integrar a API DMW a um site.

## 💡 Requisitos

PHP 7.1 ou superior

## 💻 Instalação

No diretório de seu projeto, execute em seu terminal o seguinte comando:

```shell
composer require dmw/client
```

## 🌟 Primeiros passos

```php
<?php

require __DIR__ . '/../vendor/autoload.php';

use Dmw\Client\OAuth;
use Dmw\Client\Entities\CodeGrantEntity;

session_start();

$url = (new OAuth())->codeUrl(
    (new CodeGrantEntity)
        ->setClientId(['CLIENT_ID'])
        ->setRedirectUri(['REDIRECT_URI'])
);

echo $url;

```


## 🏻 Licença de uso

Consulte o arquivo LICENSE para obter mais informações.