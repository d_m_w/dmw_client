<?php

require __DIR__ . '/../vendor/autoload.php';

use Dmw\Client\Client;
use Dmw\Client\Storage\Cookie;

//session_start();
Client::loadEnv();

$client = new Client($_ENV['CLIENT_ID'], $_ENV['CLIENT_SECRET'], new Cookie());
$profile = $client->account()->profile();

dd($profile);