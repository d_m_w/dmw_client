<?php

require __DIR__ . '/../vendor/autoload.php';

use Dmw\Client\Client;
use Dmw\Client\Storage\Cookie;
use Dmw\Client\Entities\CodeGrantEntity;
use Dmw\Client\Entities\AuthorizationCodeGrantEntity;

//session_start();
Client::loadEnv();

$client = new Client($_ENV['CLIENT_ID'], $_ENV['CLIENT_SECRET'], new Cookie());

$code = isset($_GET['code']) ? $_GET['code'] : null;
$state = isset($_GET['state']) ? $_GET['state'] : null;

if ($code && $state) {
    $token = $client->oAuth()->authorizationCode(
        (new AuthorizationCodeGrantEntity)
            ->setRedirectUri($_ENV['REDIRECT_URI'])
            ->setCode($code)
            ->setState($state)
            ->setTimeZoneMinute($_GET['time_zone_minute'] ?? null)
            ->setTimeZoneTime($_GET['time_zone_time'] ?? null)
    );

    dd($token->toArray());
}

$url = $client->oAuth()->codeUrl(
    (new CodeGrantEntity)
        ->setRedirectUri($_ENV['REDIRECT_URI'])
);

header("Location: {$url}");
die();