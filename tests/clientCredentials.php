<?php

require __DIR__ . '/../vendor/autoload.php';

use Dmw\Client\Client;
use Dmw\Client\Storage\Session;
use Dmw\Client\OAuth;
use Dmw\Client\Entities\ClientCredentialsGrantEntity;

session_start();
Client::loadEnv();

$client = new Client($_ENV['CLIENT_ID'], $_ENV['CLIENT_SECRET'], new Session());
$token = $client->oAuth()->clientCredentials(new ClientCredentialsGrantEntity);

$states = $client->dcompany()->state([
    'per_page' => 30,
    'order' => 'none'
]);

dd($states);