<?php

require __DIR__ . '/../vendor/autoload.php';

use Dmw\Client\Client;
use Dmw\Client\Storage\Cookie;
use Dmw\Client\OAuth;
use Dmw\Client\Entities\RefreshTokenGrantEntity;

session_start();
Client::loadEnv();

$refreshToken = 'def5020058aa0229917c42801d768e9cbbec8c319f2d1a87f030a2a46008e2ceb6ffef664ab184ec3f8639c0e5e02587f684aef89fa9f87b14854fc7dec8c2dca118c33271aabcbbf7c5fd7a011fd9dbcfc812254b85f55a3542727a99c141d8099947ce4e158ac60fac20297e8817159a30a1e0fec49f46d5d63955436ddbf66560e547bbed92ec936354f5487e10341691d90d3d2037e4d1dd6019a6199395c8145e0e49d9817e77c0ecaf6189a1965bdce042dbf0bf5ea891952ebe038eb6619400a3dd62c9320eac9aad032afbef08d0cd5c6000d095cfbd9ee661a97af3fa2bef05da808f5f43a3f8481badc3558e8ac7de406be7efe1942224c7f64137a0983d649b30bbdca5cf45282f73ecf775096368d98d524c7e79e89c78e763741d323e26005044be7450f8f9c794693f934c1e0572184c841ac399dc6acaca7d2e7311309cf83a783774111394ae7d62c6a997e86af06e77da04e1fa853c0a2b86b6caef4b96cb';

$client = new Client($_ENV['CLIENT_ID'], $_ENV['CLIENT_SECRET'], new Cookie());
$refresh = $client->oAuth()->refreshToken(
    (new RefreshTokenGrantEntity)
        ->setRefreshToken($refreshToken)
);

dd($refresh->toArray());