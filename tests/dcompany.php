<?php

require __DIR__ . '/../vendor/autoload.php';

use Dmw\Client\Client;
use Dmw\Client\Storage\Cookie;

Client::loadEnv();

$client = new Client($_ENV['CLIENT_ID'], $_ENV['CLIENT_SECRET'], new Cookie());
$data = $client->dcompany()->cnpj([
    'cnpj' => '46504276000108'
]);

dd($data);