<?php

namespace Dmw\Client\Entities;

final class CodeGrantEntity
{
    /**
     * @var array
     */
    private $params = [];

    /**
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->params['response_type'] = 'code';

        if (empty($params)) {
            return;
        }

        if (!empty($params['client_id'])) {
            $this->setClientId($params['client_id']);
        }
    }

    /**
     * @param string $value
     * @return self
     */
    public function setClientId(string $value): self
    {
        $this->params['client_id'] = $value;
        return $this;
    }

    /**
     * @param string $value
     * @return self
     */
    public function setRedirectUri(string $value): self
    {
        $this->params['redirect_uri'] = $value;
        return $this;
    }

    /**
     * @param string $value
     * @return self
     */
    public function setScope(string $value): self
    {
        $this->params['scope'] = $value;
        return $this;
    }

    /**
     * @param string $value
     * @return self
     */
    public function setState(string $value): self
    {
        $this->params['state'] = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function responseType(): string
    {
        return $this->params['response_type'];
    }

    /**
     * @return string
     */
    public function clientId(): string
    {
        return $this->params['client_id'] ?? '';
    }

    /**
     * @return string
     */
    public function redirectUri(): string
    {
        return $this->params['redirect_uri'] ?? '';
    }

    /**
     * @return string
     */
    public function scope(): string
    {
        return $this->params['scope'] ?? '';
    }

    /**
     * @return string
     */
    public function state(): string
    {
        return $this->params['state'] ?? '';
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->params;
    }
}
