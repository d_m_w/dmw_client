<?php

namespace Dmw\Client\Entities;

use Dmw\Client\Interfaces\ApiTokenInterface;
use Dmw\Client\Traits\DateTimeTrait;
use Dmw\Client\Exception\AppErrorException;

final class ApiTokenEntity implements ApiTokenInterface
{
    use DateTimeTrait;

    /**
     * @var string
     */
    private const TOKEN_BEARER = 'Bearer';

    /**
     * @var string
     */
    private const DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * @var string
     */
    private const TOKEN_TYPE = 'token_type';

    /**
     * @var string
     */
    private const ACCESS_TOKEN = 'access_token';

    /**
     * @var string
     */
    private const REFRESH_TOKEN = 'refresh_token';

    /**
     * @var string
     */
    private const EXPIRES_IN = 'expires_in';

    /**
     * @var string
     */
    private const EXPIRES_DATE_TIME = 'expires_date_time';

    /**
     * @var string
     */
    private const USER_TIME_ZONE_MINUTE = 'user_timezone_minute';

    /**
     * @var string
     */
    private const USER_TIME_ZONE_TIME = 'user_timezone_time';

    /**
     * @var string
     */
    private $token_type;

    /**
     * @var int
     */
    private $expires_in;

    /**
     * @var string
     */
    private $expires_date_time;

    /**
     * @var string
     */
    private $access_token;

    /**
     * @var string
     */
    private $refresh_token;

    /**
     * @var int|null
     */
    private $timezone_minute = null;

    /**
     * @var string|null
     */
    private $timezone_time = null;

    /**
     * Obtem data e hora atual
     * @return string
     */
    private function now(): string
    {
        return \date(self::DATE_FORMAT);
    }

    /**
     * @param array $params Parâmetros
     */
    public function __construct(
        array $params
    ) {
        $accessToken = $params[self::ACCESS_TOKEN] ?? null;
        $expiresIn = $params[self::EXPIRES_IN] ?? null;
        $tokenType = $params[self::TOKEN_TYPE] ?? null;
        $expiresDateTime = $params[self::EXPIRES_DATE_TIME] ?? null;
        $userTimeZoneMinute = $params[self::USER_TIME_ZONE_MINUTE] ?? null;
        $userTimeZoneTime = $params[self::USER_TIME_ZONE_TIME] ?? null;

        if (!$accessToken || !$expiresIn) {
            throw new AppErrorException("Invalid params");
        }

        if (!$tokenType) {
            $tokenType = self::TOKEN_BEARER;
        }

        $this->access_token = $accessToken;
        $this->refresh_token = $params[self::REFRESH_TOKEN] ?? null;
        $this->expires_in = $expiresIn;
        $this->token_type = $tokenType;
        $this->expires_date_time = $expiresDateTime;
        $this->timezone_minute = $userTimeZoneMinute;
        $this->timezone_time = $userTimeZoneTime;
    }

    /**
     * Configura a diferença em minutos do timezone do usuário logado para UTC
     * @param int|null $value
     */
    public function setTimezoneMinute(?int $value): void
    {
        $this->timezone_minute = $value;
    }

    /**
     * Configura a diferença no formato time do timezone do usuário logado para UTC
     * @param string|null $value
     */
    public function setTimezoneTime(?string $value): void
    {
        $this->timezone_time = $value;
    }

    /**
     * Configura data de expiração
     */
    public function setExpiresDateTime(): void
    {
        $this->expires_date_time = date('Y-m-d H:i:s', strtotime($this->now()) +  $this->expiresIn());
    }

    /**
     * Retorna o token de acesso
     * @return string
     */
    public function accessToken(): string
    {
        return $this->access_token;
    }

    /**
     * Retorna o token de atualização
     * @return string
     */
    public function refreshToken(): string
    {
        return $this->refresh_token ?: '';
    }

    /**
     * Retorna o tipo de token
     * @return string
     */
    public function tokenType(): string
    {
        return $this->token_type;
    }

    /**
     * Retorna TTL do token
     * @return int
     */
    public function expiresIn(): int
    {
        return $this->expires_in;
    }

    /**
     * Retorna a diferença em minutos do timezone do usuário logado para UTC
     * @return int|null
     */
    public function timezoneMinute(): ?int
    {
        return $this->timezone_minute;
    }

    /**
     * Retorna a diferença no formato time do timezone do usuário logado para UTC
     * @return string|null
     */
    public function timezoneTime(): ?string
    {
        return $this->timezone_time;
    }

    /**
     * Obtem data de expiração com base no TTL
     * @return string
     */
    public function expiresDateTime(): string
    {
        return $this->expires_date_time;
    }

    /**
     * Verifica se token expirou
     * @return bool
     */
    public function hasExpired(): bool
    {
        if (!$this->expiresDateTime()) {
            throw new AppErrorException("ExpiresDateTime not defined", 1);
        }

        $now = \date(self::DATE_FORMAT, \strtotime("+10 minutes", \strtotime($this->now())));
        $expire = $this->expiresDateTime();

        return strtotime($expire) < strtotime($now);
    }

    /**
     * Obtem array com os parâmetros configurados
     * @return array
     */
    public function toArray(): array
    {
        $data = [
            self::TOKEN_TYPE => $this->tokenType(),
            self::ACCESS_TOKEN => $this->accessToken(),
            self::REFRESH_TOKEN => $this->refreshToken(),
            self::EXPIRES_IN => $this->expiresIn(),
            self::EXPIRES_DATE_TIME => $this->expires_date_time,
        ];

        if ($this->timezone_minute) {
            $data[self::USER_TIME_ZONE_MINUTE] = $this->timezone_minute;
        }
        
        if ($this->timezone_time) {
            $data[self::USER_TIME_ZONE_TIME] = $this->timezone_time;
        }

        return $data;
    }
}
