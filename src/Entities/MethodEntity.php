<?php

namespace Dmw\Client\Entities;

final class MethodEntity
{
    /**
     * @var string
     */
    private $verifier;

    /**
     * @var string
     */
    private $challenge;

    /**
     * @var string
     */
    private $method;

    /**
     * @param  string $verifier   Verificador
     * @param  string $challenge  Desafio
     * @param  string $method     Método
     */
    public function __construct(
        string $verifier,
        string $challenge,
        string $method
    ) {
        $this->verifier = $verifier;
        $this->challenge = $challenge;
        $this->method = $method;
    }

    /**
     * Obtem o verificador
     * @return string
     */
    public function verifier(): string
    {
        return $this->verifier;
    }

    /**
     * Obtem o desafio
     * @return string
     */
    public function challenge(): string
    {
        return $this->challenge;
    }

    /**
     * Obtem o método
     * @return string
     */
    public function method(): string
    {
        return $this->method;
    }
}
