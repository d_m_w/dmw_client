<?php

namespace Dmw\Client\Entities;

final class AuthorizationCodeGrantEntity
{
    /**
     * @var array
     */
    private $params = [];

    /**
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->params['grant_type'] = 'authorization_code';

        if (empty($params)) {
            return;
        }

        if (!empty($params['client_id'])) {
            $this->setClientId($params['client_id']);
        }

        if (!empty($params['client_secret'])) {
            $this->setClientSecret($params['client_secret']);
        }
    }

    /**
     * @param string $value
     * @return self
     */
    public function setClientId(string $value): self
    {
        $this->params['client_id'] = $value;
        return $this;
    }

    /**
     * @param string $value
     * @return self
     */
    public function setRedirectUri(string $value): self
    {
        $this->params['redirect_uri'] = $value;
        return $this;
    }

    /**
     * @param string $value
     * @return self
     */
    public function setClientSecret(string $value): self
    {
        $this->params['client_secret'] = $value;
        return $this;
    }

    /**
     * @param string $value
     * @return self
     */
    public function setCode(string $value): self
    {
        $this->params['code'] = $value;
        return $this;
    }

    /**
     * @param string $value
     * @return self
     */
    public function setState(string $value): self
    {
        $this->params['state'] = $value;
        return $this;
    }

    /**
     * @param int|null $value
     * @return self
     */
    public function setTimeZoneMinute(?int $value): self
    {
        if (!$value) {
            return $this;
        }

        $this->params['time_zone_minute'] = $value;
        return $this;
    }

    /**
     * @param string|null $value
     * @return self
     */
    public function setTimeZoneTime(?string $value): self
    {
        if (!$value) {
            return $this;
        }

        $this->params['time_zone_time'] = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function grantType(): string
    {
        return $this->params['grant_type'];
    }

    /**
     * @return string
     */
    public function clientId(): string
    {
        return $this->params['client_id'] ?? '';
    }

    /**
     * @return string
     */
    public function clientSecret(): string
    {
        return $this->params['client_secret'] ?? '';
    }

    /**
     * @return string
     */
    public function redirectUri(): string
    {
        return $this->params['redirect_uri'] ?? '';
    }

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->params['code'] ?? '';
    }

    /**
     * @return string
     */
    public function state(): string
    {
        return $this->params['state'] ?? '';
    }

    /**
     * @return int|null
     */
    public function timeZoneMinute(): ?int
    {
        return $this->params['time_zone_minute'] ?? null;
    }

    /**
     * @return string|null
     */
    public function timeZoneTime(): ?string
    {
        return $this->params['time_zone_time'] ?? null;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->params;
    }
}
