<?php

namespace Dmw\Client\Entities;

final class PasswordGrantEntity
{
    /**
     * @var array
     */
    private $params = [];

    /**
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->params['grant_type'] = 'password';

        if (empty($params)) {
            return;
        }

        if (!empty($params['client_id'])) {
            $this->setClientId($params['client_id']);
        }

        if (!empty($params['client_secret'])) {
            $this->setClientSecret($params['client_secret']);
        }
    }

    /**
     * @param string $value
     * @return self
     */
    public function setClientId(string $value): self
    {
        $this->params['client_id'] = $value;
        return $this;
    }

    /**
     * @param string $value
     * @return self
     */
    public function setClientSecret(string $value): self
    {
        $this->params['client_secret'] = $value;
        return $this;
    }

    /**
     * @param string $value
     * @return self
     */
    public function setScope(string $value): self
    {
        $this->params['scope'] = $value;
        return $this;
    }

    /**
     * @param string $value
     * @return self
     */
    public function setUserName(string $value): self
    {
        $this->params['username'] = $value;
        return $this;
    }

    /**
     * @param string $value
     * @return self
     */
    public function setPassword(string $value): self
    {
        $this->params['password'] = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function grantType(): string
    {
        return $this->params['grant_type'];
    }

    /**
     * @return string
     */
    public function clientId(): string
    {
        return $this->params['client_id'] ?? '';
    }

    /**
     * @return string
     */
    public function clientSecret(): string
    {
        return $this->params['client_secret'] ?? '';
    }

    /**
     * @return string
     */
    public function scope(): string
    {
        return $this->params['scope'] ?? '';
    }

    /**
     * @return string
     */
    public function userName(): string
    {
        return $this->params['username'] ?? '';
    }

    /**
     * @return string
     */
    public function password(): string
    {
        return $this->params['password'] ?? '';
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->params;
    }
}
