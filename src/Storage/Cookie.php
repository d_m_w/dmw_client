<?php

namespace Dmw\Client\Storage;

use Dmw\Client\Interfaces\StorageInterface;

class Cookie implements StorageInterface
{
    /**
     * @var array
     */
    private $params = [];

    /**
     * Verifica se está executando em ambiente HTTPs
     * @return bool
     */
    private function isHttps(): bool
    {
        if (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off') {
            return true;
        } elseif ((isset($_SERVER['HTTP_X_FORWARDED_PROTO'])) &&
            (strtolower($_SERVER['HTTP_X_FORWARDED_PROTO']) === 'https')
        ) {
            return true;
        } elseif ((!empty($_SERVER['HTTP_FRONT_END_HTTPS'])) &&
            (strtolower($_SERVER['HTTP_FRONT_END_HTTPS']) !== 'off')
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param string $samesite
     */
    public function __construct(
        string $samesite = 'LAX'
    ) {
        $secure = $this->isHttps() ? true : false;
        $samesite = $secure ? $samesite : '';

        $this->params = [
            'path' => '/',
            'secure' => $secure,
            'httponly' => true,
            'samesite' => $samesite
        ];
    }

    /**
     * Configura TTL em segundos
     * @param int $value
     */
    public function setTtl(
        int $value
    ): void {
        $this->params['expires'] = time() + $value;
    }

    /**
     * Configura valor
     * @param string $key
     * @param string $value
     */
    public function set(
        string $key,
        string $value
    ): void {
        if (version_compare(PHP_VERSION, '7.3.0') >= 0) {
            unset($this->params['samesite']);
        }

        \setCookie($key, $value, $this->params);
    }

    /**
     * Obtem valor configurado
     * @param  string $key
     * @return string
     */
    public function get(
        string $key
    ): string {
        return $_COOKIE[$key] ?? '';
    }

    /**
     * Remove valor configurado
     * @param  string $key
     * @return bool
     */
    public function delete(
        string $key
    ): bool {
        unset($_COOKIE[$key]);
        return true;
    }
}
