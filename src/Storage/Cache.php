<?php

namespace Dmw\Client\Storage;

use Dmw\Client\Interfaces\StorageInterface;
use Psr\SimpleCache\CacheInterface;

class Cache implements StorageInterface
{
    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var int
     */
    private $ttl;

    /**
     * @param CacheInterface $cache
     */
    public function __construct(
        CacheInterface $cache
    ) {
        $this->cache = $cache;
    }

    /**
     * Configura TTL em segundos
     * @param int $value
     */
    public function setTtl(
        int $value
    ): void {
        $this->ttl = $value;
    }

    /**
     * Configura valor
     * @param string $key
     * @param string $value
     */
    public function set(
        string $key,
        string $value
    ): void {
        if ($this->get($key)) {
            $this->delete($key);
        }

        $this->cache->set($key, $value, $this->ttl);
    }

    /**
     * Obtem valor configurado
     * @param  string $key
     * @return string
     */
    public function get(
        string $key
    ): string {
        return $this->cache->has($key) ? $this->cache->get($key) : '';
    }

    /**
     * Remove valor configurado
     * @param  string $key
     * @return bool
     */
    public function delete(
        string $key
    ): bool {
        return $this->cache->has($key) ? $this->cache->delete($key) : true;
    }
}
