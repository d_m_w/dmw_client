<?php

namespace Dmw\Client\Storage;

use Dmw\Client\Interfaces\StorageInterface;

class Session implements StorageInterface
{
    /**
     * @var bool
     */
    private $session_active;

    public function __construct()
    {
        $this->session_active = session_status() == PHP_SESSION_ACTIVE;
    }

    /**
     * Configura TTL em segundos
     * @param int $value
     */
    public function setTtl(
        int $value
    ): void {
        //Compatibilidade
    }
    
    /**
     * Verifica se sessão está ativa
     * @return bool
     */
    public function active(): bool
    {
        return $this->session_active;
    }

    /**
     * Configura valor
     * @param string $key
     * @param string $value
     */
    public function set(
        string $key,
        string $value
    ): void {
        if (!$this->active()) {
            return;
        }

        if ($this->get($key)) {
            $this->delete($key);
        }

        $_SESSION[$key] = $value;
    }

    /**
     * Obtem valor configurado
     * @param  string $key
     * @return string
     */
    public function get(
        string $key
    ): string {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }

        return '';
    }

    /**
     * Remove valor configurado
     * @param  string $key
     * @return bool
     */
    public function delete(
        string $key
    ): bool {
        unset($_SESSION[$key]);
        return true;
    }
}
