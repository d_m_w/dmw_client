<?php

namespace Dmw\Client\Exception;

use Exception;

class HttpClientException extends Exception
{
    /**
     * @var int
     */
    protected $code = 502;

    /**
     * @param string    $message
     * @param int       $code
     * @param Exception $previous
     */
    public function __construct(
        string $message,
        int $code = 502,
        Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
