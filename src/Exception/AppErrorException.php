<?php

namespace Dmw\Client\Exception;

use Exception;

class AppErrorException extends Exception
{
    /**
     * @var int
     */
    protected $code = 502;
}
