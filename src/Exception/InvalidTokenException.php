<?php

namespace Dmw\Client\Exception;

use Exception;

class InvalidTokenException extends Exception
{
  /**
   * @var int
   */
    protected $code = 502;
}
