<?php

namespace Dmw\Client\Exception;

use Exception;

class InvalidCodeException extends Exception
{
  /**
   * @var int
   */
    protected $code = 502;
}
