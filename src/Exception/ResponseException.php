<?php

namespace Dmw\Client\Exception;

use Exception;

class ResponseException extends Exception
{
    /**
     * @param string    $message
     * @param int       $code
     * @param Exception $previous
     */
    public function __construct(
        string $message,
        int $code,
        Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
