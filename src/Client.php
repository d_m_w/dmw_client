<?php

namespace Dmw\Client;

use Throwable;
use Dmw\Client\Traits\JwtTrait;
use Dmw\Client\Interfaces\StorageInterface;
use Dmw\Client\Entities\ApiTokenEntity;
use Dmw\Client\Entities\RefreshTokenGrantEntity;
use Dmw\Client\Security\ApiToken;
use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Endpoints\OAuth;
use Dmw\Client\Endpoints\Account;
use Dmw\Client\Endpoints\DCollect;
use Dmw\Client\Endpoints\DCompany;
use Dmw\Client\Endpoints\DContacts;
use Dmw\Client\Endpoints\DNotes;
use Dmw\Client\Endpoints\DMoney;
use Dmw\Client\Endpoints\DManager;
use Dmw\Client\Endpoints\DPayment;
use Dmw\Client\Endpoints\DCalendar;
use Dmw\Client\Endpoints\DStock;
use Dotenv\Dotenv;
use Dotenv\Repository\RepositoryBuilder;
use Dotenv\Repository\Adapter\EnvConstAdapter;
use Dotenv\Repository\Adapter\PutenvAdapter;

final class Client
{
    use JwtTrait;

    /**
     * @var ApiToken
     */
    private $token;

    /**
     * @var Endpoint
     */
    private $endpoint;

    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $clientSecret;

    /**
     * @param string           $clientId
     * @param string           $clientSecret
     * @param StorageInterface $storage
     */
    public function __construct(
        string $clientId,
        string $clientSecret,
        StorageInterface $storage
    ) {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->storage = $storage;
        $this->token = new ApiToken($this->clientId, $this->storage);

        $this->endpoint = new Endpoint();
        $this->endpoint->setApiToken($this->token);
    }

    /**
     * Carrega parâmetros do arquivo .env
     */
    public static function loadEnv(): void
    {
        $repository = RepositoryBuilder::createWithNoAdapters()
            ->addAdapter(EnvConstAdapter::class)
            ->addWriter(PutenvAdapter::class)
            ->immutable()
            ->make();

        $dotenv = Dotenv::create($repository, __DIR__ . "/..");
        $dotenv->load();
    }
    
    /**
     * @return OAuth
     */
    public function oAuth(): OAuth
    {
        return new OAuth(
            $this->clientId,
            $this->clientSecret,
            $this->storage
        );
    }

    /**
     * @return Account
     */
    public function account(): Account
    {
        return new Account($this->endpoint, $this->getToken());
    }

    /**
     * @return DCompany
     */
    public function dcompany(): DCompany
    {
        return new DCompany($this->endpoint, $this->getToken());
    }

    /**
     * @return DContacts
     */
    public function dcontacts(): DContacts
    {
        return new DContacts($this->endpoint, $this->getToken());
    }

    /**
     * @return DNotes
     */
    public function dnotes(): DNotes
    {
        return new DNotes($this->endpoint, $this->getToken());
    }

    /**
     * @return DMoney
     */
    public function dmoney(): DMoney
    {
        return new DMoney($this->endpoint, $this->getToken());
    }

    /**
     * @return DManager
     */
    public function dmanager(): DManager
    {
        return new DManager($this->endpoint, $this->getToken());
    }

    /**
     * @return DPayment
     */
    public function dpayment(): DPayment
    {
        return new DPayment($this->endpoint, $this->getToken());
    }

    /**
     * @return DCollect
     */
    public function dcollect(): DCollect
    {
        return new DCollect($this->endpoint, $this->getToken());
    }

    /**
     * @return DCalendar
     */
    public function dcalendar(): DCalendar
    {
        return new DCalendar($this->endpoint, $this->getToken());
    }

    /**
     * @return DStock
     */
    public function dstock(): DStock
    {
        return new DStock($this->endpoint, $this->getToken());
    }

    /**
     * Obtem token
     * @return ApiToken
     */
    public function getTokenData(): ApiToken
    {
        return $this->token;
    }

    /**
     * Remove tokens do armazenamento
     */
    public function clearToken(): void
    {
        $this->getTokenData()->clear();
    }

    /**
     * Atualiza token
     * @param string $accessToken
     * @param string $refreshToken
     * @return ApiTokenEntity|null
     */
    private function refreshToken(
        string $accessToken,
        string $refreshToken
    ): ?ApiTokenEntity {
        try {
            $data = $this->getJwtTokenData($accessToken);
            $token = $this->oAuth()->refreshToken(
                (new RefreshTokenGrantEntity)
                    ->setRefreshToken($refreshToken)
                    ->setScope(implode(' ', $data['scopes']))
            );
        } catch (Throwable $th) {
            $this->getTokenData()->clear();
            $token = null;
        }

        return $token;
    }

    /**
     * Configura token
     * @return ApiTokenEntity|null
     */
    public function getToken(): ?ApiTokenEntity 
    {
        $token = $this->getTokenData()->get();
        if ($token && $token->refreshToken() && $token->hasExpired()) {
            return $this->refreshToken($token->accessToken(), $token->refreshToken());
        }

        return $token;
    }
}
