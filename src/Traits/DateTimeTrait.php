<?php

namespace Dmw\Client\Traits;

trait DateTimeTrait
{
    /**
     * Adiciona zeros a esquerda
     * @param  float  $text Número no formato string
     * @param  int    $size Quantidade de zeros
     * @return string
     */
    public static function leftZeros(
        float $text,
        int $size
    ): string {
        return str_pad(strval($text), $size, '0', STR_PAD_LEFT);
    }

    /**
     * Verifica se é uma data válida
     * @param  string $text Data
     * @return string|false
     */
    public function validDate(
        string $text
    ) {
        if (!$text) {
            return false;
        }

        // Caso seja informado uma data do tipo DATETIME - yyyy-mm-dd 00:00:00. Transforma para DATE - yyyy-mm-dd
        $data = mb_substr($text, 0, 10);

        // Se a data estiver no formato brasileiro: dd/mm/aaaa. Converte-a para o padrão americano: aaaa-mm-dd
        if (preg_match('@/@', $data) == 1) {
            $data = implode("-", array_reverse(explode("/", $data)));
        }

        $res = false;
        $dta = explode("-", $data);

        $a = 0;
        $m = 0;
        $d = 0;

        if (count($dta) == 3) {
            if (strlen($dta[0]) == 4) { //formato americano
                $a = intval($dta[0]);
                $m = intval($dta[1]);
                $d = intval($dta[2]);
            } else {
                $a = intval($dta[2]);
                $m = intval($dta[1]);
                $d = intval($dta[0]);
            }
            $dta = checkdate($m, $d, $a);
        } else {
            $dta = explode("/", $data);

            if (count($dta) == 3) {
                if (strlen($dta[0]) == 4) { //formato americano
                    $a = intval($dta[0]);
                    $m = intval($dta[1]);
                    $d = intval($dta[2]);
                } else {
                    $a = intval($dta[2]);
                    $m = intval($dta[1]);
                    $d = intval($dta[0]);
                }
                $dta = checkdate($m, $d, $a);
            } else {
                $dta = false;
            }
        }

        if ($dta == true && $m && $d && $a) {
            $res = date("Y-m-d", mktime(0, 0, 0, $m, $d, $a));
        }

        //verifica o horário
        $time = mb_substr($text, 11);

        if ($time) {
            $tme = explode(":", $time);

            if (count($tme) == 3) { //00:00:00
                $h = $this->leftZeros(intval($tme[0]), 2);
                $i = $this->leftZeros(intval($tme[1]), 2);
                $s = $this->leftZeros(intval($tme[2]), 2);
            } elseif (count($tme) == 2) { //00:00
                $h = $this->leftZeros(intval($tme[0]), 2);
                $i = $this->leftZeros(intval($tme[1]), 2);
                $s = '00';
            } else {
                $h = '';
                $i = '';
                $s = '';
            }

            if ($h && $i && $s) {
                $time = strtotime($h . ":" . $i . ":" . $s);

                if ($time) {
                    if (intval($h) > 12) {
                        $time = date("H:i:s", $time);
                    } else {
                        $time = date("h:i:s", $time);
                    }
                } else {
                    $time = '';
                }

                $time = ' ' . $time;
            } else {
                $time = '';
            }
        }

        return $res . $time;
    }
}
