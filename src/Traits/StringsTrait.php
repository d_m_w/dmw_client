<?php

namespace Dmw\Client\Traits;

use Dmw\Client\Exception\AppErrorException;

trait StringsTrait
{
    /**
     * Obtem string randômica segura
     * @param  integer $length Quantidade de caracteres
     * @return string
     */
    private function randomBytes(
        int $length = 16
    ): string {
        if (PHP_MAJOR_VERSION >= 7) {
            $bytes = random_bytes($length);
        } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $bytes = openssl_random_pseudo_bytes($length, $strong);
            if (!$bytes || !$strong) {
                throw new AppErrorException('Failed to generate random string');
            }
        } else {
            throw new AppErrorException('OpenSSL extension is required for PHP 5 users');
        }

        return $bytes;
    }

    /**
     * Obtem string randômica
     * @param  integer $length Quantidade de caracteres
     * @return string
     */
    protected function random(
        int $length = 16
    ): string {
        $string = '';
        while (($len = mb_strlen($string)) < $length) {
            $size    = $length - $len;
            $bytes   = $this->randomBytes($size);
            $string .= mb_substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
        }

        return $string;
    }

    /**
     * Verifica se é um Json
     * @param  string $data Json
     * @return bool
     */
    public static function isJson(
        string $data
    ): bool {
        if ($data &&
            is_string($data) &&
            is_array(json_decode($data, true)) &&
            (json_last_error() == JSON_ERROR_NONE)
        ) {
            return true;
        } else {
            return false;
        }
    }
}
