<?php

namespace Dmw\Client\Traits;

use Lcobucci\JWT\Token\Parser;
use Lcobucci\JWT\Encoding\JoseEncoder;

trait JwtTrait
{
    /**
     * Obtém dados inseridos no token
     * @param string $accessToken
     * @return array
     */
    public function getJwtTokenData(
        string $accessToken
    ): array {
        return (new Parser(new JoseEncoder()))->parse($accessToken)->claims()->all();
    }
}
