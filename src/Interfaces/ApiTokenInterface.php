<?php

namespace Dmw\Client\Interfaces;

interface ApiTokenInterface
{
    /**
     * Configura a diferença em minutos do timezone do usuário logado para UTC
     * @param int|null $value
     */
    public function setTimezoneMinute(?int $value): void;

    /**
     * Configura a diferença no formato time do timezone do usuário logado para UTC
     * @param string|null $value
     */
    public function setTimezoneTime(?string $value): void;

    /**
     * Configura data de expiração
     */
    public function setExpiresDateTime(): void;

    /**
     * Retorna o token de acesso
     * @return string
     */
    public function accessToken(): string;

    /**
     * Retorna o token de atualização
     * @return string
     */
    public function refreshToken(): string;

    /**
     * Retorna o tipo de token
     * @return string
     */
    public function tokenType(): string;

    /**
     * Retorna TTL do token
     * @return int
     */
    public function expiresIn(): int;

    /**
     * Obtem data de expiração com base no TTL
     * @return string
     */
    public function expiresDateTime(): string;

    /**
     * Retorna a diferença em minutos do timezone do usuário logado para UTC
     * @return int|null
     */
    public function timezoneMinute(): ?int;

    /**
     * Retorna a diferença no formato time do timezone do usuário logado para UTC
     * @return string|null
     */
    public function timezoneTime(): ?string;
        
    /**
     * Verifica se token expirou
     * @return bool
     */
    public function hasExpired(): bool;

    /**
     * Obtem array com os parâmetros configurados
     * @return array
     */
    public function toArray(): array;
}
