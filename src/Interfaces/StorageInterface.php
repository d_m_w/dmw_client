<?php

namespace Dmw\Client\Interfaces;

interface StorageInterface
{
    /**
     * Configura valor
     * @param string $key
     * @param string $value
     */
    public function set(
        string $key,
        string $value
    ): void;

    /**
     * Obtem valor configurado
     * @param  string $key
     * @return string
     */
    public function get(
        string $key
    ): string;

    /**
     * Remove valor configurado
     * @param  string $key
     * @return bool
     */
    public function delete(
        string $key
    ): bool;

    /**
     * Configura TTL em segundos
     * @param int $value
     */
    public function setTtl(
        int $value
    ): void;
}
