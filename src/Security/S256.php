<?php

namespace Dmw\Client\Security;

use Dmw\Client\Exception\AppErrorException;

final class S256
{
    /**
     * @var string
     */
    private $value;

    /**
     * @var string
     */
    private $verifier;

    /**
     * @var string
     */
    private $challenge;

    /**
     * Codifica url em base64
     * @param  string $value
     * @return string
     */
    private function base64urlEncode(
        string $value
    ): string {
        $base64 = base64_encode($value);
        $base64 = trim($base64, "=");
        $base64url = strtr($base64, '+/', '-_');

        return $base64url;
    }

    public function __construct()
    {
        try {
            $this->value = bin2hex(openssl_random_pseudo_bytes(32));
            $this->verifier = $this->base64urlEncode(pack('H*', $this->value));
            $this->challenge = $this->base64urlEncode(
                pack('H*', hash('sha256', $this->verifier))
            );
        } catch (\Exception $e) {
            throw new AppErrorException('Failed to generate random string');
        }
    }

    /**
     * Obtem verificador
     * @return string
     */
    public function verifier(): string
    {
        return $this->verifier;
    }

    /**
     * Obtem desafio
     * @return string
     */
    public function challenge(): string
    {
        return $this->challenge;
    }
}
