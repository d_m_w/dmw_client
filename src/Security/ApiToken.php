<?php

namespace Dmw\Client\Security;

use Dmw\Client\Storage\Cookie;
use Dmw\Client\Storage\Session;
use Dmw\Client\Entities\ApiTokenEntity;
use Dmw\Client\Interfaces\StorageInterface;
use Psr\SimpleCache\CacheInterface;

final class ApiToken
{
    /**
     * @var string
     */
    const KEY_NAME = '_dmw_cd';

    /**
     * Um mês tempo do refresh token
     * @var int
     */
    const TOKEN_EXPIRES = 2628000;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $clientId;
    
    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @param string           $clientId
     * @param StorageInterface $storage
     */
    public function __construct(
        string $clientId,
        StorageInterface $storage
    ) {
        $this->clientId = $clientId;
        $this->key = self::KEY_NAME;
        $this->storage = $storage;
    }

    /**
     * Configura dados do token de API
     * @param ApiTokenEntity $entity
     * @return self
     */
    public function set(
        ApiTokenEntity $entity
    ): self {
        $tmp = $this->get();
        if ($tmp) {
            $timezoneMinute = $entity->timezoneMinute();
            $timezoneTime = $entity->timezoneTime();

            if (!$timezoneMinute) {
                $entity->setTimezoneMinute($tmp->timezoneMinute());
            }

            if (!$timezoneTime) {
                $entity->setTimezoneTime($tmp->timezoneTime());
            }
        }

        $value = $entity->toArray();
        if (!$this->storage instanceof Session) {
            $expiresIn = $value['refresh_token'] ? $value['expires_in'] - self::TOKEN_EXPIRES : $value['expires_in'];
            $this->storage->setTtl($expiresIn);
        }

        if ($this->storage instanceof Cookie) {
            $this->storage->set($this->getCookieKey(), json_encode($value));
            return $this;
        }

        $data = $this->getData();
        if (!empty($data[$this->clientId])) {
            $this->storage->set($this->key, json_encode($data));
            return $this;
        }

        $data[$this->clientId] = $value;
        $this->storage->set($this->key, json_encode($data));
        return $this;
    }

    /**
     * Obtem chave de cache para Cookie
     * @return string
     */
    private function getCookieKey(): string
    {
        return self::KEY_NAME . '_' . $this->clientId;
    }

    /**
     * Obtem dados do token de API do Cookie
     * @return ApiTokenEntity|null
     */
    private function getFromCookie(): ?ApiTokenEntity
    {
        $key = $this->getCookieKey();
        $value = $this->storage->get($key);
        if (!$value) {
            return null;
        }

        return new ApiTokenEntity(json_decode($value, true));
    }

    /**
     * Obtem dados do token de API de todos clientIds
     * @return array|null
     */
    private function getData(): ?array
    {
        $value = $this->storage->get($this->key);
        if (!$value) {
            return null;
        }

        return json_decode($value, true);
    }

    /**
     * Obtem dados do token de API
     * @return ApiTokenEntity|null
     */
    public function get(): ?ApiTokenEntity
    {
        if ($this->storage instanceof Cookie) {
            return $this->getFromCookie();
        }

        $value = $this->storage->get($this->key);
        if (!$value) {
            return null;
        }

        $data = json_decode($value, true);
        $value = $data[$this->clientId] ?? null;
        if (!$value) {
            return null;
        }

        return new ApiTokenEntity($value);
    }

    /**
     * Remove valor token
     */
    public function delete(): void
    {
        $data = json_decode($this->storage->get($this->key), true);

        unset($data[$this->clientId]);
        
        $this->storage->set($this->key, json_encode($data));
    }

    /**
     * Remove todos os tokens
     * @return bool
     */
    public function clear(): bool
    {
        return $this->storage->delete($this->key);
    }
}
