<?php

namespace Dmw\Client\Security;

use Dmw\Client\Security\S256;
use Dmw\Client\Entities\MethodEntity;
use Dmw\Client\Traits\StringsTrait;
use Dmw\Client\Interfaces\StorageInterface;
use Psr\SimpleCache\CacheInterface;

final class Code
{
    use StringsTrait;

    /**
     * @var string
     */
    const METHOD_S256 = 'S256';

    /**
     * @var string
     */
    const METHOD_PLAIN = 'plain';

    /**
     * @var integer
     */
    const KEY_TTL = 3600;

    /**
     * @var string
     */
    const KEY_NAME = '_dmw_c_';

    /**
     * @var string
     */
    private $key;
    
    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @var string
     */
    private $method;

    /**
     * @var string|null
     */
    private $data = null;

    /**
     * @param string           $clientId
     * @param StorageInterface $storage
     * @param string           $method
     */
    public function __construct(
        string $clientId,
        StorageInterface $storage,
        string $method = 'S256'
    ) {
        if (!in_array($method, [self::METHOD_S256, self::METHOD_PLAIN])) {
            throw new \InvalidArgumentException("Invalid method", 1);
        }

        $this->method = $method;
        $this->key = self::KEY_NAME . $clientId;

        $this->storage = $storage;
        $this->storage->setTtl(self::KEY_TTL);
    }

    /**
     * Configura código
     * @return self
     */
    public function set(): self
    {
        $data = $this->get();
        if ($data) {
            return $this;
        }

        $method = self::METHOD_PLAIN;
        $verifier  = $this->random(40);
        $challenge = $verifier;

        if ($this->method == self::METHOD_S256) {
            $s256   = new S256;
            $method = self::METHOD_S256;
            $verifier  = $s256->verifier();
            $challenge = $s256->challenge();
        }

        $value = \serialize(new MethodEntity($verifier, $challenge, $method));
        $this->storage->set($this->key, $value);
        $this->data = $value;

        return $this;
    }

    /**
     * Obtem valor código
     * @return object|null
     */
    public function get()
    {
        $value = $this->data ?: $this->storage->get($this->key);

        return $value ? \unserialize($value) : null;
    }

    /**
     * Remove valor código
     * @return bool
     */
    public function delete(): bool
    {
        return $this->storage->delete($this->key);
    }
}
