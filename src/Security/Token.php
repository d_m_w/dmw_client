<?php

namespace Dmw\Client\Security;

use Dmw\Client\Traits\StringsTrait;
use Dmw\Client\Interfaces\StorageInterface;
use Psr\SimpleCache\CacheInterface;

final class Token
{
    use StringsTrait;

    /**
     * @var integer
     */
    const KEY_TTL = 3600;

    /**
     * @var string
     */
    const KEY_NAME = '_dmw_t_';

    /**
     * @var string
     */
    private $key;

    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @var string|null
     */
    private $data = null;

    /**
     * @param string           $clientId
     * @param StorageInterface $storage
     */
    public function __construct(
        string $clientId,
        StorageInterface $storage
    ) {
        $this->key = self::KEY_NAME . $clientId;
        
        $this->storage = $storage;
        $this->storage->setTtl(self::KEY_TTL);
    }

    /**
     * Configura token
     * @return self
     */
    public function set(): self
    {
        $value = $this->get() ?: $this->random(40);
        $this->storage->set($this->key, $value);
        $this->data = $value;

        return $this;
    }

    /**
     * Obtem valor token
     * @return string
     */
    public function get(): string
    {
        if ($this->data) {
            return $this->data;
        }

        return $this->storage->get($this->key);
    }

    /**
     * Remove valor token
     * @return bool
     */
    public function delete(): bool
    {
        return $this->storage->delete($this->key);
    }
}
