<?php

namespace Dmw\Client\Endpoints\DManager;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class CustomerService
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }

    /**
     * Obtém lista de atendimentos
     * @param array $params
     * @return mixed
     */
    public function index(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/customer-service",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém atendimento
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function show(
        int $id,
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/customer-service/{$id}",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Insere novo atendimento
     * @param array $params
     * @return mixed
     */
    public function store(
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/customer-service",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Atualiza atendimento
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function update(
        int $id,
        array $params
    ) {
        return $this->client->request(
            Endpoint::PUT,
            "{$this->url}/v2/customer-service/{$id}",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém lista de serviços do atendimento
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function serviceIndex(
        int $id,
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/customer-service/{$id}/services",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém serviço de atendimento
     * @param int   $id
     * @param int   $idService
     * @param array $params
     * @return mixed
     */
    public function serviceShow(
        int $id,
        int $idService,
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/customer-service/{$id}/services/{$idService}",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Insere novo serviço no atendimento
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function serviceStore(
        int $id,
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/customer-service/{$id}/services",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Atualiza serviço no atendimento
     * @param int   $id
     * @param int   $idService
     * @param array $params
     * @return mixed
     */
    public function serviceUpdate(
        int $id,
        int $idService,
        array $params
    ) {
        return $this->client->request(
            Endpoint::PUT,
            "{$this->url}/v2/customer-service/{$id}/services/{$idService}",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém lista de produtos do atendimento
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function productIndex(
        int $id,
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/customer-service/{$id}/products",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém produto de atendimento
     * @param int   $id
     * @param int   $idProduct
     * @param array $params
     * @return mixed
     */
    public function productShow(
        int $id,
        int $idProduct,
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/customer-service/{$id}/products/{$idProduct}",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Insere novo produto no atendimento
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function productStore(
        int $id,
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/customer-service/{$id}/products",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Atualiza produto no atendimento
     * @param int   $id
     * @param int   $idProduct
     * @param array $params
     * @return mixed
     */
    public function productUpdate(
        int $id,
        int $idProduct,
        array $params
    ) {
        return $this->client->request(
            Endpoint::PUT,
            "{$this->url}/v2/customer-service/{$id}/products/{$idProduct}",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém lista de pagamentos do atendimento
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function paymentIndex(
        int $id,
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/customer-service/{$id}/payments",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém pagamento de atendimento
     * @param int   $id
     * @param int   $idPayment
     * @param array $params
     * @return mixed
     */
    public function paymentShow(
        int $id,
        int $idPayment,
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/customer-service/{$id}/payments/{$idPayment}",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Insere novo pagamento no atendimento
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function paymentStore(
        int $id,
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/customer-service/{$id}/payments",
            $params,
            $this->token->accessToken()
        );
    }
}