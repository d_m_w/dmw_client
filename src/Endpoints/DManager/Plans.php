<?php

namespace Dmw\Client\Endpoints\DManager;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class Plans
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }

    /**
     * Obtém lista de planos
     * @param array $params
     * @return mixed
     */
    public function index(
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/plan",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém plano
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function show(
        int $id,
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/plan/{$id}",
            $params,
            $this->token->accessToken()
        );
    }
}
