<?php

namespace Dmw\Client\Endpoints;

use Dmw\Client\Endpoints\DContacts\BankData;
use Dmw\Client\Endpoints\DContacts\Contacts;
use Dmw\Client\Endpoints\DContacts\Others;
use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class DContacts
{
    /**
     * @var string
     */
    private const API_URL = 'https://dcontacts.dmw.net.br/api';

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @var string
     */
    private $url;

    /**
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->client = $client;
        $this->token = $token;
        $this->url = $this->getUrl();
    }

    /**
     * @return BankData
     */
    public function bankData(): BankData
    {
        return new BankData($this->url, $this->client, $this->token);
    }

    /**
     * @return Contacts
     */
    public function contacts(): Contacts
    {
        return new Contacts($this->url, $this->client, $this->token);
    }

    /**
     * @return Others
     */
    public function others(): Others
    {
        return new Others($this->url, $this->client, $this->token);
    }

    /**
     * Obtém url da API
     * @return string
     */
    private function getUrl(): string
    {
        return $_ENV['DCONTACTS_API_URL'] ?? self::API_URL;
    }
}
