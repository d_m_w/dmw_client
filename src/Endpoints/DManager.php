<?php

namespace Dmw\Client\Endpoints;

use Dmw\Client\Endpoints\DManager\Plans;
use Dmw\Client\Endpoints\DManager\Services;
use Dmw\Client\Endpoints\DManager\Employees;
use Dmw\Client\Endpoints\DManager\EmployeesServices;
use Dmw\Client\Endpoints\DManager\CustomerService;
use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class DManager
{
    /**
     * @var string
     */
    private const API_URL = 'https://dmanager.dmw.net.br/api';

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @var string
     */
    private $url;

    /**
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->client = $client;
        $this->token = $token;
        $this->url = $this->getUrl();
    }

    /**
     * @return Plans
     */
    public function plans(): Plans
    {
        return new Plans($this->url, $this->client, $this->token);
    }

    /**
     * @return Services
     */
    public function services(): Services
    {
        return new Services($this->url, $this->client, $this->token);
    }

    /**
     * @return Employees
     */
    public function employees(): Employees
    {
        return new Employees($this->url, $this->client, $this->token);
    }

    /**
     * @return EmployeesServices
     */
    public function employeesServices(): EmployeesServices
    {
        return new EmployeesServices($this->url, $this->client, $this->token);
    }

    /**
     * @return CustomerService
     */
    public function customerService(): CustomerService
    {
        return new CustomerService($this->url, $this->client, $this->token);
    }

    /**
     * Obtém url da API
     * @return string
     */
    private function getUrl(): string
    {
        return $_ENV['DMANAGER_API_URL'] ?? self::API_URL;
    }
}
