<?php

namespace Dmw\Client\Endpoints\DCollect;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class Veiculo
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }

    /**
     * Obtém lista de veículos
     * @param array $params
     * @return mixed
     */
    public function index(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/veiculo",
            $params,
            $this->token->accessToken()
        );
    }
    
    /**
     * Obtém dados do veículo
     * @param array  $params
     * @return mixed
     */
    public function show(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/veiculo/show",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Insere novo veículo
     * @param array $params
     * @return mixed
     */
    public function store(
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/veiculo",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Atualiza veículo
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function update(
        int $id,
        array $params
    ) {
        return $this->client->request(
            Endpoint::PUT,
            "{$this->url}/v2/veiculo/{$id}",
            $params,
            $this->token->accessToken()
        );
    }
}
