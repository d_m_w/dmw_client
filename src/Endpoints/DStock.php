<?php

namespace Dmw\Client\Endpoints;

use Dmw\Client\Endpoints\DStock\Groups;
use Dmw\Client\Endpoints\DStock\Products;
use Dmw\Client\Endpoints\DStock\ProductsPrice;
use Dmw\Client\Endpoints\DStock\ProductsPriceItem;
use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class DStock
{
    /**
     * @var string
     */
    private const API_URL = 'https://dstock.dmw.net.br/api';

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @var string
     */
    private $url;

    /**
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->client = $client;
        $this->token = $token;
        $this->url = $this->getUrl();
    }

    /**
     * @return Groups
     */
    public function groups(): Groups
    {
        return new Groups($this->url, $this->client, $this->token);
    }

    /**
     * @return Products
     */
    public function products(): Products
    {
        return new Products($this->url, $this->client, $this->token);
    }

    /**
     * @return ProductsPrice
     */
    public function productsPrice(): ProductsPrice
    {
        return new ProductsPrice($this->url, $this->client, $this->token);
    }

    /**
     * @return ProductsPriceItem
     */
    public function productsPriceItem(): ProductsPriceItem
    {
        return new ProductsPriceItem($this->url, $this->client, $this->token);
    }

    /**
     * Obtém url da API
     * @return string
     */
    private function getUrl(): string
    {
        return $_ENV['DSTOCK_API_URL'] ?? self::API_URL;
    }
}
