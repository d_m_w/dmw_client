<?php

namespace Dmw\Client\Endpoints;

use Dmw\Client\Endpoints\DPayment\Signature;
use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class DPayment
{
    /**
     * @var string
     */
    private const API_URL = 'https://dpayment.dmw.net.br/api';

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @var string
     */
    private $url;

    /**
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->client = $client;
        $this->token = $token;
        $this->url = $this->getUrl();
    }

    /**
     * @return Signature
     */
    public function signature(): Signature
    {
        return new Signature($this->url, $this->client, $this->token);
    }

    /**
     * Obtém url da API
     * @return string
     */
    private function getUrl(): string
    {
        return $_ENV['DPAYMENT_API_URL'] ?? self::API_URL;
    }
}
