<?php

namespace Dmw\Client\Endpoints;

use Dmw\Client\Endpoints\DCompany\Onboarding;
use Dmw\Client\Endpoints\DCompany\Others;
use Dmw\Client\Endpoints\DCompany\Services;
use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class DCompany
{
    /**
     * @var string
     */
    private const API_URL = 'https://dcompany.dmw.net.br/api';

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @var string
     */
    private $url;

    /**
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->client = $client;
        $this->token = $token;
        $this->url = $this->getUrl();
    }

    /**
     * @return Others
     */
    public function others(): Others
    {
        return new Others($this->url, $this->client, $this->token);
    }

    /**
     * @return Services
     */
    public function services(): Services
    {
        return new Services($this->url, $this->client, $this->token);
    }

    /**
     * @return Onboarding
     */
    public function onboarding(): Onboarding
    {
        return new Onboarding($this->url, $this->client, $this->token);
    }

    /**
     * Obtém url da API
     * @return string
     */
    private function getUrl(): string
    {
        return $_ENV['DCOMPANY_API_URL'] ?? self::API_URL;
    }
}
