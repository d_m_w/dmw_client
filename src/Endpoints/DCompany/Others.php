<?php

namespace Dmw\Client\Endpoints\DCompany;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class Others
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }
    
    /**
     * Obtém listagem de países
     * @param array $params
     * @return mixed
     */
    public function country(
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/country",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém listagem de UFs
     * @param array $params
     * @return mixed
     */
    public function state(
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/state",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém listagem de cidades
     * @param array $params
     * @return mixed
     */
    public function city(
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/city",
            $params,
            $this->token->accessToken()
        );
    }

        /**
     * Obtém listagem de CNAEs
     * @param array $params
     * @return mixed
     */
    public function cnae(
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/cnae",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém listagem IBPTAX
     * @param array $params
     * @return mixed
     */
    public function ibptax(
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/ibptax",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém listagem de unidades de medida
     * @param array $params
     * @return mixed
     */
    public function unitMeasurement(
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/unit-measurement",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém dados de unidade de medida
     * @param int $id
     * @return mixed
     */
    public function unitMeasurementShow(
        int $id
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/unit-measurement/{$id}",
            [],
            $this->token->accessToken()
        );
    }    

    /**
     * Obtém listagem de serviços
     * @param array $params
     * @return mixed
     */
    public function listServices(
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/list-services",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém dados de serviço
     * @param int $id
     * @return mixed
     */
    public function listServicesShow(
        int $id
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/list-services/{$id}",
            [],
            $this->token->accessToken()
        );
    }

    /**
     * Obtém listagem de bancos
     * @param array $params
     * @return mixed
     */
    public function bank(
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/bank",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém listagem de tipos de contas
     * @param array $params
     * @return mixed
     */
    public function bankAccountType(
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/bank-account-type",
            $params,
            $this->token->accessToken()
        );
    }
}
