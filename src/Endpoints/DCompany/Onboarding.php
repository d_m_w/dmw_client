<?php

namespace Dmw\Client\Endpoints\DCompany;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class Onboarding
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }

    /**
     * Obtém onboarding
     * @param array $params
     * @return mixed
     */
    public function index(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/onboarding",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Configura check
     * @param array $params
     * @return mixed
     */
    public function checked(
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/onboarding",
            $params,
            $this->token->accessToken()
        );
    }
}
