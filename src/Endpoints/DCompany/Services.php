<?php

namespace Dmw\Client\Endpoints\DCompany;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class Services
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }
    
    /**
     * Consulta dados através do CNPJ
     * @param array $params
     * @return mixed
     */
    public function cnpj(
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/service/cnpj",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Consulta dados através do CEP
     * @param array $params
     * @return mixed
     */
    public function cep(
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/service/cep",
            $params,
            $this->token->accessToken()
        );
    }
}
