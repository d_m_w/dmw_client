<?php

namespace Dmw\Client\Endpoints;

use Dmw\Client\Endpoints\DCollect\StatusCarga;
use Dmw\Client\Endpoints\DCollect\StatusAgendamento;
use Dmw\Client\Endpoints\DCollect\Carga;
use Dmw\Client\Endpoints\DCollect\TipoCarga;
use Dmw\Client\Endpoints\DCollect\TipoCarroceria;
use Dmw\Client\Endpoints\DCollect\TipoOperacao;
use Dmw\Client\Endpoints\DCollect\TipoVeiculo;
use Dmw\Client\Endpoints\DCollect\VeiculoCor;
use Dmw\Client\Endpoints\DCollect\VeiculoMarca;
use Dmw\Client\Endpoints\DCollect\VeiculoModelo;
use Dmw\Client\Endpoints\DCollect\Veiculo;
use Dmw\Client\Endpoints\DCollect\Reboque;
use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class DCollect
{
    /**
     * @var string
     */
    private const API_URL = 'https://dcollect.dmw.net.br/api';

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @var string
     */
    private $url;

    /**
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->client = $client;
        $this->token = $token;
        $this->url = $this->getUrl();
    }

    /**
     * @return Carga
     */
    public function carga(): Carga
    {
        return new Carga($this->url, $this->client, $this->token);
    }

    /**
     * @return TipoCarga
     */
    public function tipoCarga(): TipoCarga
    {
        return new TipoCarga($this->url, $this->client, $this->token);
    }

    /**
     * @return TipoCarroceria
     */
    public function tipoCarroceria(): TipoCarroceria
    {
        return new TipoCarroceria($this->url, $this->client, $this->token);
    }

    /**
     * @return TipoOperacao
     */
    public function tipoOperacao(): TipoOperacao
    {
        return new TipoOperacao($this->url, $this->client, $this->token);
    }

    /**
     * @return TipoVeiculo
     */
    public function tipoVeiculo(): TipoVeiculo
    {
        return new TipoVeiculo($this->url, $this->client, $this->token);
    }

    /**
     * @return StatusCarga
     */
    public function statusCarga(): StatusCarga
    {
        return new StatusCarga($this->url, $this->client, $this->token);
    }

    /**
     * @return StatusAgendamento
     */
    public function statusAgendamento(): StatusAgendamento
    {
        return new StatusAgendamento($this->url, $this->client, $this->token);
    }

    /**
     * @return VeiculoCor
     */
    public function veiculoCor(): VeiculoCor
    {
        return new VeiculoCor($this->url, $this->client, $this->token);
    }

    /**
     * @return VeiculoMarca
     */
    public function veiculoMarca(): VeiculoMarca
    {
        return new VeiculoMarca($this->url, $this->client, $this->token);
    }

    /**
     * @return VeiculoModelo
     */
    public function veiculoModelo(): VeiculoModelo
    {
        return new VeiculoModelo($this->url, $this->client, $this->token);
    }

    /**
     * @return Veiculo
     */
    public function veiculo(): Veiculo
    {
        return new Veiculo($this->url, $this->client, $this->token);
    }

    /**
     * @return Reboque
     */
    public function reboque(): Reboque
    {
        return new Reboque($this->url, $this->client, $this->token);
    }

    /**
     * Obtém url da API
     * @return string
     */
    private function getUrl(): string
    {
        return $_ENV['DCOLLECT_API_URL'] ?? self::API_URL;
    }
}
