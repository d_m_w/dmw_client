<?php

namespace Dmw\Client\Endpoints\DStock;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class ProductsPriceItem
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }

    /**
     * Obtém lista de produtos
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function index(
        int $id,
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/products-price/items/{$id}",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém dados produto
     * @param int   $precoProdutoId
     * @param int   $produtoComposicaoId
     * @param array $params
     * @return mixed
     */
    public function show(
        int $precoProdutoId,
        int $produtoComposicaoId,
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/products-price/items/{$precoProdutoId}/{$produtoComposicaoId}",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Atualiza preço de produto
     * @param array $params
     * @return mixed
     */
    public function update(
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/products-price/items",
            $params,
            $this->token->accessToken()
        );
    }
}
