<?php

namespace Dmw\Client\Endpoints\DContacts;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class Contacts
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }

    /**
     * Obtém lista de entidades
     * @param array $params
     * @return mixed
     */
    public function index(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/contacts",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém dados de entidade
     * @param string $type
     * @param array  $params
     * @return mixed
     */
    public function show(
        string $type,
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/contacts/{$type}",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Insere novo contato
     * @param array $params
     * @return mixed
     */
    public function store(
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/contacts",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Atualiza contato
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function update(
        int $id,
        array $params
    ) {
        return $this->client->request(
            Endpoint::PUT,
            "{$this->url}/v2/contacts/{$id}",
            $params,
            $this->token->accessToken()
        );
    }
}
