<?php

namespace Dmw\Client\Endpoints\DContacts;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class Others
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }

    /**
     * Obtém lista de gêneros
     * @param array $params
     * @return mixed
     */
    public function gender(
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/gender",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém tipos de telefone
     * @param array $params
     * @return mixed
     */
    public function phoneType(
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/phone/type",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém tipos de e-mail
     * @param array $params
     * @return mixed
     */
    public function emailType(
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/email/type",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém tipos de link social
     * @param array $params
     * @return mixed
     */
    public function linkType(
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/link/type",
            $params,
            $this->token->accessToken()
        );
    }
}
