<?php

namespace Dmw\Client\Endpoints\DContacts;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class BankData
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }

    /**
     * Obtém dado bancário
     * @param array $params
     * @return mixed
     */
    public function show(
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/bank-data",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Insere novo dado bancário
     * @param array $params
     * @return mixed
     */
    public function store(
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/bank-data",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Atualiza dado bancário
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function update(
        int $id,
        array $params
    ) {
        return $this->client->request(
            Endpoint::PUT,
            "{$this->url}/v2/bank-data/{$id}",
            $params,
            $this->token->accessToken()
        );
    }
}
