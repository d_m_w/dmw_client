<?php

namespace Dmw\Client\Endpoints;

use Dmw\Client\Endpoints\DCalendar\Participants;
use Dmw\Client\Endpoints\DCalendar\Events;
use Dmw\Client\Endpoints\DCalendar\Calendars;
use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class DCalendar
{
    /**
     * @var string
     */
    private const API_URL = 'https://dcalendar.dmw.net.br/api';

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @var string
     */
    private $url;

    /**
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->client = $client;
        $this->token = $token;
        $this->url = $this->getUrl();
    }

    /**
     * @return Participants
     */
    public function participants(): Participants
    {
        return new Participants($this->url, $this->client, $this->token);
    }

    /**
     * @return Events
     */
    public function events(): Events
    {
        return new Events($this->url, $this->client, $this->token);
    }

    /**
     * @return Calendars
     */
    public function calendars(): Calendars
    {
        return new Calendars($this->url, $this->client, $this->token);
    }

    /**
     * Obtém url da API
     * @return string
     */
    private function getUrl(): string
    {
        return $_ENV['DCALENDAR_API_URL'] ?? self::API_URL;
    }
}
