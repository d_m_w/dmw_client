<?php

namespace Dmw\Client\Endpoints;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class Account
{
    /**
     * @var string
     */
    protected const OAUTH2_API_URL = 'https://app.dmw.net.br';

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @var string
     */
    private $url;

    /**
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->client = $client;
        $this->token = $token;
        $this->url = $this->getUrl();
    }

    /**
     * Obtem perfil do usuário
     * @return object
     */
    public function profile(): object
    {
        $data = $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/account/profile",
            null,
            $this->token->accessToken()
        );

        $timezoneMinute = $this->token->timezoneMinute();
        $timezoneTime = $this->token->timezoneTime();

        if ($timezoneMinute) {
            $data->timezone_minute = $timezoneMinute;
        }

        if ($timezoneTime) {
            $data->timezone_time = $timezoneTime;
        }

        return $data;
    }

    /**
     * Atualiza licença
     * @param array $params
     * @return mixed
     */
    public function signatureSave(
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/signature/save",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtem URL da API
     * @return string
     */
    protected function getUrl(): string
    {
        return $_ENV['OAUTH2_API_URL'] ?? self::OAUTH2_API_URL;
    }
}
