<?php

namespace Dmw\Client\Endpoints;

use Dmw\Client\Endpoints\DMoney\Budgets;
use Dmw\Client\Endpoints\DMoney\Finances;
use Dmw\Client\Endpoints\DMoney\Import;
use Dmw\Client\Endpoints\DMoney\Others;
use Dmw\Client\Endpoints\DMoney\Wallets;
use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class DMoney
{
    /**
     * @var string
     */
    private const API_URL = 'https://dmoney.dmw.net.br/api';

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @var string
     */
    private $url;

    /**
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->client = $client;
        $this->token = $token;
        $this->url = $this->getUrl();
    }

    /**
     * @return Budgets
     */
    public function budgets(): Budgets
    {
        return new Budgets($this->url, $this->client, $this->token);
    }

    /**
     * @return Finances
     */
    public function finances(): Finances
    {
        return new Finances($this->url, $this->client, $this->token);
    }

    /**
     * @return Import
     */
    public function import(): Import
    {
        return new Import($this->url, $this->client, $this->token);
    }

    /**
     * @return Others
     */
    public function others(): Others
    {
        return new Others($this->url, $this->client, $this->token);
    }

    /**
     * @return Wallets
     */
    public function wallets(): Wallets
    {
        return new Wallets($this->url, $this->client, $this->token);
    }

    /**
     * Obtém url da API
     * @return string
     */
    private function getUrl(): string
    {
        return $_ENV['DMONEY_API_URL'] ?? self::API_URL;
    }
}
