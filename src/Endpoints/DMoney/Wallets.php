<?php

namespace Dmw\Client\Endpoints\DMoney;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class Wallets
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }

    /**
     * Obtém lista de carteiras
     * @param array $params
     * @return mixed
     */
    public function index(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/wallets",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém lista de carteiras de pagamento
     * @param array $params
     * @return mixed
     */
    public function indexPayment(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/wallets/payment",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém carteira
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function show(
        int $id,
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/wallets/{$id}",
            $params,
            $this->token->accessToken()
        );
    }
}
