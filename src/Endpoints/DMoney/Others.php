<?php

namespace Dmw\Client\Endpoints\DMoney;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class Others
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }

    /**
     * Obtém lista de grupos de contas
     * @param array $params
     * @return mixed
     */
    public function indexGroups(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/groups",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém lista de contas
     * @param array $params
     * @return mixed
     */
    public function indexFinancialAccounts(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/financial-accounts",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém conta
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function showFinancialAccounts(
        int $id,
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/financial-accounts/{$id}",
            $params,
            $this->token->accessToken()
        );
    }    

    /**
     * Obtém lista de centro de custos
     * @param array $params
     * @return mixed
     */
    public function indexCostCenters(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/cost-centers",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Calcula multa e juros
     * @param array $params
     * @return mixed
     */
    public function penaltiesInterest(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/penalties-interest",
            $params,
            $this->token->accessToken()
        );
    }
}
