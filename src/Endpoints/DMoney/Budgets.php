<?php

namespace Dmw\Client\Endpoints\DMoney;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class Budgets
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }

    /**
     * Obtém dados de orçamento
     * @param array $params
     * @return mixed
     */
    public function show(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/budgets",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Insere novo orçamento
     * @param array $params
     * @return mixed
     */
    public function store(
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/budgets",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Atualiza orçamento
     * @param array $params
     * @return mixed
     */
    public function update(
        array $params
    ) {
        return $this->client->request(
            Endpoint::PUT,
            "{$this->url}/v2/budgets",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Insere item no orçamento
     * @param array $params
     * @return mixed
     */
    public function storeItem(
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/budgets/item",
            $params,
            $this->token->accessToken()
        );
    }
    
    /**
     * Remove item do orçamento
     * @param array $params
     * @return mixed
     */
    public function destroyItem(
        array $params
    ) {
        return $this->client->request(
            Endpoint::DELETE,
            "{$this->url}/v2/budgets/item",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém totais do orçamento
     * @param array $params
     * @return mixed
     */
    public function sumValue(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/budgets/values",
            $params,
            $this->token->accessToken()
        );
    }
}
