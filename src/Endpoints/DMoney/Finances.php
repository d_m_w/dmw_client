<?php

namespace Dmw\Client\Endpoints\DMoney;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class Finances
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }

    /**
     * Atualiza lançamento para marcado
     * @param array $params
     * @return mixed
     */
    public function updateChecked(
        array $params
    ) {
        return $this->client->request(
            Endpoint::PUT,
            "{$this->url}/v2/finances/checked",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Atualiza lançamento para desmarcado
     * @param array $params
     * @return mixed
     */
    public function updateUnchecked(
        array $params
    ) {
        return $this->client->request(
            Endpoint::PUT,
            "{$this->url}/v2/finances/unchecked",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém dados de lançamento
     * @param array $params
     * @return mixed
     */
    public function show(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/finances",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Exclui lançamento
     * @param int   $origemId
     * @param array $params
     * @return mixed
     */
    public function delete(
        int $origemId,
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::DELETE,
            "{$this->url}/v2/finances/{$origemId}",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Insere lançamento
     * @param array $params
     * @return mixed
     */
    public function store(
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/finances",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Atualiza lançamento
     * @param array $params
     * @return mixed
     */
    public function update(
        array $params
    ) {
        return $this->client->request(
            Endpoint::PUT,
            "{$this->url}/v2/finances",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Atualiza lançamento recorrente
     * @param array $params
     * @return mixed
     */
    public function updateRecurrent(
        array $params
    ) {
        return $this->client->request(
            Endpoint::PUT,
            "{$this->url}/v2/finances/recurrent",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Insere tranferência
     * @param array $params
     * @return mixed
     */
    public function storeTransfer(
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/finances/transfer",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Insere pagamento
     * @param array $params
     * @return mixed
     */
    public function storePayment(
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/finances/payment",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Marca para notificar sobre o lançamento
     * @param array $params
     * @return mixed
     */
    public function updateNotify(
        array $params
    ) {
        return $this->client->request(
            Endpoint::PUT,
            "{$this->url}/v2/finances/notify",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Marca para não notificar sobre o lançamento
     * @param array $params
     * @return mixed
     */
    public function updateNotNotify(
        array $params
    ) {
        return $this->client->request(
            Endpoint::PUT,
            "{$this->url}/v2/finances/not-notify",
            $params,
            $this->token->accessToken()
        );
    }
}
