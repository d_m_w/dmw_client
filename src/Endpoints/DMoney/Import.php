<?php

namespace Dmw\Client\Endpoints\DMoney;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class Import
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }

    /**
     * Obtém dado de regra de importação
     * @param array $params
     * @return mixed
     */
    public function showRule(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/import/rules",
            $params,
            $this->token->accessToken()
        );
    }
    
    /**
     * Insere nova regra de importação
     * @param array $params
     * @return mixed
     */
    public function storeRule(
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/import/rules",
            $params,
            $this->token->accessToken()
        );
    }
    
    /**
     * Atualiza regra de importação
     * @param array $params
     * @return mixed
     */
    public function updateRule(
        array $params
    ) {
        return $this->client->request(
            Endpoint::PUT,
            "{$this->url}/v2/import/rules",
            $params,
            $this->token->accessToken()
        );
    }
}
