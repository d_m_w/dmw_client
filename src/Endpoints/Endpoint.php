<?php

namespace Dmw\Client\Endpoints;

use Exception;
use Dmw\Client\Traits\StringsTrait;
use Dmw\Client\Security\ApiToken;
use Dmw\Client\Exception\HttpClientException;
use Dmw\Client\Exception\ResponseException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class Endpoint
{
    use StringsTrait;

    /**
     * @var string
     */
    public const GET = 'get';

    /**
     * @var string
     */
    public const POST = 'post';

    /**
     * @var string
     */
    public const PUT = 'put';

    /**
     * @var string
     */
    public const DELETE = 'delete';

    /**
     * @var string
     */
    private const PARAMS_QUERY = 'query';

    /**
     * @var string
     */
    private const PARAMS_JSON = 'json';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var ApiToken
     */
    private $token;

    public function __construct()
    {
        $this->client = new Client([
            'verify' => false
        ]);
    }

    /**
     * Configura API token
     * @param ApiToken $value
     */
    public function setApiToken(
        ApiToken $value
    ): void {
        $this->token = $value;
    }
    
    /**
     * Executa a request
     * @param string      $method
     * @param string      $uri
     * @param array|null  $params
     * @param string|null $token
     * @return mixed
     */
    public function request(
        string $method,
        string $uri,
        ?array $params = null,
        ?string $token = ''
    ) {
        $options = [];
        if ($params) {
            $key = strtolower($method) == self::GET ? self::PARAMS_QUERY : self::PARAMS_JSON;
            $options[$key] = $params;

            if ($key == self::PARAMS_JSON) {
                $options['headers']['Content-Type'] = 'application/json';
            }
        }

        if ($token) {
            $options['headers']['authorization'] = "Bearer {$token}";
        }
        
        try {
            $result = $this->client->request($method, $uri, $options);
            $response = $result->getBody()->getContents();
            $statusCode = $result->getStatusCode();

            if ($statusCode > 299) {
                throw new Exception($response, $statusCode);
            }

            if ($response && $this->isJson($response)) {
                $response = \json_decode($response);
            }

            return $response;
        } catch (ClientException $e) {
            if ($this->token && $e->getCode() == 401) {
                $this->token->delete();
            }

            $response = $e->getResponse()->getBody()->getContents();
            throw new HttpClientException($response, $e->getCode());
        } catch (ServerException $e) {
            if ($this->token && $e->getCode() == 401) {
                $this->token->delete();
            }

            $response = $e->getResponse()->getBody()->getContents();
            throw new HttpClientException($response, $e->getCode());
        } catch (Exception $e) {
            throw new ResponseException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Verifica se é um Json
     * @param  mixed $data Json
     * @return bool
     */
    private function isJson(
        $data
    ): bool {
        return ($data && is_string($data) && is_array(json_decode($data, true)) &&
            (json_last_error() == JSON_ERROR_NONE)
        ) ? true : false;
    }
}
