<?php

namespace Dmw\Client\Endpoints;

use Dmw\Client\Endpoints\DNotes\Groups;
use Dmw\Client\Endpoints\DNotes\Notes;
use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class DNotes
{
    /**
     * @var string
     */
    private const API_URL = 'https://dnotes.dmw.net.br/api';

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @var string
     */
    private $url;

    /**
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->client = $client;
        $this->token = $token;
        $this->url = $this->getUrl();
    }

    /**
     * @return Groups
     */
    public function groups(): Groups
    {
        return new Groups($this->url, $this->client, $this->token);
    }

    /**
     * @return Notes
     */
    public function notes(): Notes
    {
        return new Notes($this->url, $this->client, $this->token);
    }

    /**
     * Obtém url da API
     * @return string
     */
    private function getUrl(): string
    {
        return $_ENV['DNOTES_API_URL'] ?? self::API_URL;
    }
}
