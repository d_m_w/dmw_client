<?php

namespace Dmw\Client\Endpoints;

use Dmw\Client\Interfaces\StorageInterface;
use Dmw\Client\Security\Code;
use Dmw\Client\Security\Token;
use Dmw\Client\Security\ApiToken;
use Dmw\Client\Entities\CodeGrantEntity;
use Dmw\Client\Entities\RefreshTokenGrantEntity;
use Dmw\Client\Entities\PasswordGrantEntity;
use Dmw\Client\Entities\ImplicitGrantEntity;
use Dmw\Client\Entities\ClientCredentialsGrantEntity;
use Dmw\Client\Entities\AuthorizationCodeGrantEntity;
use Dmw\Client\Entities\ApiTokenEntity;
use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Exception\AppErrorException;
use Dmw\Client\Exception\InvalidCodeException;

class OAuth
{
    /**
     * @var string
     */
    private const OAUTH2_AUTH_URL = 'https://account.dmw.net.br';

    /**
     * @var string
     */
    private const OAUTH2_URI_AUTHORIZE = '/api/oauth/v2/authorize';
    
    /**
     * @var string
     */
    private const OAUTH2_URI_ACCESS_TOKEN = '/api/oauth/v2/access-token';

    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $clientSecret;

    /**
     * @param string           $clientId
     * @param string           $clientSecret
     * @param StorageInterface $storage
     */
    public function __construct(
        string $clientId,
        string $clientSecret,
        StorageInterface $storage
    ) {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->storage = $storage;
    }

    /**
     * Obtem classe de armazenamento
     * @return StorageInterface
     */
    protected function getStorage(): StorageInterface
    {
        return $this->storage;
    }

    /**
     * Obtem url para geração do código de autorização
     * @param  CodeGrantEntity $grant
     * @return string
     */
    public function codeUrl(
        CodeGrantEntity $grant
    ): string {
        $token = (new Token($this->clientId, $this->getStorage()))->set();
        $secureCode = (new Code($this->clientId, $this->getStorage()))->set();

        $grant->setClientId($this->clientId);

        $secureCode = $secureCode->get();
        $params = $grant->toArray();
        $params['code_challenge_method'] = $secureCode->method();
        $params['code_challenge'] = $secureCode->challenge();

        if (empty($params['state'])) {
            $params['state'] = $token->get();
        }

        $params = \http_build_query($params);
        return $this->getAuthUrl(self::OAUTH2_URI_AUTHORIZE) . '?' . \urldecode($params);
    }

    /**
     * Obter token através do authorization_code
     * @param AuthorizationCodeGrantEntity $grant
     * @return ApiTokenEntity
     */
    public function authorizationCode(
        AuthorizationCodeGrantEntity $grant
    ): ApiTokenEntity {
        $token = new Token($this->clientId, $this->getStorage());
        if ($token->get() != $grant->state()) {
            $token->delete();
            throw new InvalidCodeException('Invalid state');
        }

        $secureCode = new Code($this->clientId, $this->getStorage());

        $token->delete();
        $grant->setClientId($this->clientId);
        $grant->setClientSecret($this->clientSecret);

        $params = $grant->toArray();
        $params['code'] = \urldecode($grant->code());
        $params['code_verifier'] = $secureCode->get()->verifier();

        $response = $this->requestOAuth(
            Endpoint::POST,
            self::OAUTH2_URI_ACCESS_TOKEN,
            $params
        );

        if ($response instanceof ApiTokenEntity) {
            $secureCode->delete();
            $token->delete();
        }

        return $response;
    }

    /**
     * Obter token através do client_credentials
     * @param ClientCredentialsGrantEntity $grant
     * @return ApiTokenEntity
     */
    public function clientCredentials(
        ClientCredentialsGrantEntity $grant
    ): ApiTokenEntity {
        $grant->setClientId($this->clientId);
        $grant->setClientSecret($this->clientSecret);

        return $this->requestOAuth(
            Endpoint::POST,
            self::OAUTH2_URI_ACCESS_TOKEN,
            $grant->toArray()
        );
    }

    /**
     * Obtem token através do password
     * @param PasswordGrantEntity $grant
     * @return ApiTokenEntity
     */
    public function password(
        PasswordGrantEntity $grant
    ): ApiTokenEntity {
        $grant->setClientId($this->clientId);
        $grant->setClientSecret($this->clientSecret);
        
        return $this->requestOAuth(
            Endpoint::POST,
            self::OAUTH2_URI_ACCESS_TOKEN,
            $grant->toArray()
        );
    }

    /**
     * Obtem token através do implicit
     * @param ImplicitGrantEntity $grant
     * @return ApiTokenEntity
     */
    public function implicitGrant(
        ImplicitGrantEntity $grant
    ): ApiTokenEntity {
        $grant->setClientId($this->clientId);

        return $this->requestOAuth(
            Endpoint::POST,
            self::OAUTH2_URI_ACCESS_TOKEN,
            $grant->toArray()
        );
    }

    /**
     * Obtem token através do refresh token
     * @param RefreshTokenGrantEntity $grant
     * @return ApiTokenEntity
     */
    public function refreshToken(
        RefreshTokenGrantEntity $grant
    ): ApiTokenEntity {
        $grant->setClientId($this->clientId);
        $grant->setClientSecret($this->clientSecret);

        return $this->requestOAuth(
            Endpoint::POST,
            self::OAUTH2_URI_ACCESS_TOKEN,
            $grant->toArray()
        );
    }

    /**
     * Executa a requisição no OAuth Server
     * @param string      $method
     * @param string      $uri
     * @param array|null  $params
     * @param string|null $token
     * @return ApiTokenEntity
     */
    private function requestOAuth(
        string $method,
        string $uri,
        ?array $params = null,
        ?string $token = ''
    ): ApiTokenEntity {
        $timeZoneMinute = $params['time_zone_minute'] ?? null;
        $timeZoneTime = $params['time_zone_time'] ?? null;
        
        if ($timeZoneMinute) {
            unset($params['$time_zone_minute']);
        }

        if ($timeZoneTime) {
            unset($params['time_zone_time']);
        }

        $apiToken = new ApiToken($this->clientId, $this->getStorage());
        $endpoint = new Endpoint();
        $endpoint->setApiToken($apiToken);

        $response = $endpoint->request(
            $method,
            $this->getAuthUrl($uri),
            $params,
            $token
        );

        if (!is_object($response)) {
            throw new AppErrorException("Invalid response");
        }

        if (!isset($response->refresh_token)) {
            $response->refresh_token = null;
        }

        $apiTokenEntity = new ApiTokenEntity((array)$response);
        $apiTokenEntity->setTimezoneMinute($timeZoneMinute);
        $apiTokenEntity->setTimezoneTime($timeZoneTime);
        $apiTokenEntity->setExpiresDateTime();

        $apiToken->set($apiTokenEntity);

        return $apiTokenEntity;
    }

    /**
     * Obtem URL do aplicativo de autenticação
     * @param string $uri
     * @return string
     */
    private function getAuthUrl(
        string $uri = ''
    ): string {
        return ($_ENV['OAUTH2_AUTH_URL'] ?? self::OAUTH2_AUTH_URL) . $uri;
    }
}
