<?php

namespace Dmw\Client\Endpoints\DCalendar;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class Events
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }

    /**
     * Obtém lista de eventos
     * @param array $params
     * @return mixed
     */
    public function index(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/events",
            $params,
            $this->token->accessToken()
        );
    }
    
    /**
     * Insere novo evento
     * @param array $params
     * @return mixed
     */
    public function store(
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/events",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Atualiza evento
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function update(
        int $id,
        array $params
    ) {
        return $this->client->request(
            Endpoint::PUT,
            "{$this->url}/v2/events/{$id}",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Exclui evento
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function delete(
        int $id,
        array $params = []
    ) {
        return $this->client->request(
            Endpoint::DELETE,
            "{$this->url}/v2/events/{$id}",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém lista de horários disponíveis
     * @param array $params
     * @return mixed
     */
    public function schedule(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/events/schedules-period",
            $params,
            $this->token->accessToken()
        );
    }
}
