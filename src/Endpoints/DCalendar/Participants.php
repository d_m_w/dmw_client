<?php

namespace Dmw\Client\Endpoints\DCalendar;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class Participants
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }
    
    /**
     * Insere novo participante ao evento
     * @param array $params
     * @return mixed
     */
    public function store(
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/events/participants",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Remove participante do evento
     * @param int $entidadeId
     * @return mixed
     */
    public function delete(
        int $entidadeId
    ) {
        return $this->client->request(
            Endpoint::DELETE,
            "{$this->url}/v2/events/participants/{$entidadeId}",
            [],
            $this->token->accessToken()
        );
    }
}
