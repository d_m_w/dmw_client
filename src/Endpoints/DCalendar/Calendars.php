<?php

namespace Dmw\Client\Endpoints\DCalendar;

use Dmw\Client\Endpoints\Endpoint;
use Dmw\Client\Entities\ApiTokenEntity;

class Calendars
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Endpoint
     */
    private $client;

    /**
     * @var ApiTokenEntity
     */
    private $token;

    /**
     * @param string         $url
     * @param Endpoint       $client
     * @param ApiTokenEntity $token
     */
    public function __construct(
        string $url,
        Endpoint $client,
        ApiTokenEntity $token
    ) {
        $this->url = $url;
        $this->client = $client;
        $this->token = $token;
    }

    /**
     * Obtém lista de agendas
     * @param array $params
     * @return mixed
     */
    public function index(
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/calendars",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Obtém agenda
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function show(
        int $id,
        array $params
    ) {
        return $this->client->request(
            Endpoint::GET,
            "{$this->url}/v2/calendars/{$id}",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Insere nova agenda
     * @param array $params
     * @return mixed
     */
    public function store(
        array $params
    ) {
        return $this->client->request(
            Endpoint::POST,
            "{$this->url}/v2/calendars",
            $params,
            $this->token->accessToken()
        );
    }

    /**
     * Atualiza agenda
     * @param int   $id
     * @param array $params
     * @return mixed
     */
    public function update(
        int $id,
        array $params
    ) {
        return $this->client->request(
            Endpoint::PUT,
            "{$this->url}/v2/calendars/{$id}",
            $params,
            $this->token->accessToken()
        );
    }
}
